﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Desafio
{
    class Program
    {

        private static List<PriceList> _listaPrecos = new List<PriceList>();

        static void Main(string[] args)
        {
            string opcao = null;

            //Monta lista com valores Default.
            System.Console.WriteLine(System.Environment.NewLine);
            MontaLista();

            //Exibe a lista.
            ListaPrecoAtual();

            while (true)
            {
                //Monta Layout para administrativo
                opcao = ExibeMenu();

                //Realiza Execução de acordo com o item selecionado pelo usuário.
                ExecutaAcao(opcao);
            }            
        }

        /// <summary>
        /// Executa a ação selecionada pelo usúario.
        /// </summary>
        /// <param name="opcao"></param>
        private static void ExecutaAcao(string opcao)
        {

            switch (int.Parse(opcao))
            {
                case 2://Para exibir lista de itens
                    System.Console.WriteLine(Environment.NewLine);
                    ListaPrecoAtual();
                    break;
                case 3://Para realizar uma compra
                    List<PurchaseList> itens = new List<PurchaseList>();
                    itens=SelecionaItens();
                    CalculaItensSelecionados(itens);
                    break;
                case 4://Para finalizar o sistema
                    Environment.Exit(0);
                    break;
                case 1://Para alteração de valores de Special Price A
                    AjustaPrecos();                    
                    break;
                default:
                    System.Console.WriteLine("Opçao inválida!");
                    break;
            }

        }

        /// <summary>
        /// Realiza o calculo e aplica as regras
        /// </summary>
        /// <param name="itens"></param>
        private static void CalculaItensSelecionados(List<PurchaseList> itens)
        {
            decimal valorTotalCompra=0;

            //Procura o valor dos itens em lista de Preços
            foreach (var iten in itens)
            {
                foreach (var precos in _listaPrecos)
                {
                    if (iten.item.Equals(precos.item))
                    {
                        //Verifica se existe Special Price.
                        if (!string.IsNullOrWhiteSpace(precos.tantosPorQtd) && !string.IsNullOrWhiteSpace(precos.tantosPorValor))
                        {
                            //Verifica se cai na regra Special Price
                            if ( int.Parse(iten.qtdItem) >= int.Parse(precos.tantosPorQtd))
                            {
                                //Calcula itens sobre Special Price
                                int tot = int.Parse(iten.qtdItem)  / int.Parse(precos.tantosPorQtd);
                                valorTotalCompra += tot * int.Parse(precos.tantosPorValor);

                                //Calcula residual sobre o valor de Unit Price
                                int resto = int.Parse(iten.qtdItem) % int.Parse(precos.tantosPorQtd);
                                valorTotalCompra += resto * int.Parse(precos.unitPrice);

                                break;
                            }
                            else{
                                //Tem Special Price, mas não entra na regra por conta da quantidade.
                                valorTotalCompra +=  int.Parse(iten.qtdItem) * int.Parse(precos.unitPrice);
                                break;
                            }
                        }
                        else{
                            if (!string.IsNullOrWhiteSpace(precos.unitPrice))
                                valorTotalCompra += int.Parse(iten.qtdItem) * int.Parse(precos.unitPrice);
                            else
                                valorTotalCompra += 0;
                            break;
                        }
                    }
                }
            }
            
            string valorFinal= string.Format("{0:0,0.00}", valorTotalCompra);

            System.Console.WriteLine(Environment.NewLine);
            System.Console.WriteLine("Itens X Quantidade");

            foreach (var it in itens)
            {
                System.Console.WriteLine(string.Format("Item: {0} x {1}",it.item,it.qtdItem));
            }
            System.Console.WriteLine("Valor Total: "+valorFinal);
        }

        /// <summary>
        /// Armazena os itens selecionados
        /// </summary>
        private static List<PurchaseList> SelecionaItens()
        {
            bool isNovoItem = true;
            List<PurchaseList> listaCompras = new List<PurchaseList>();
            
            while (isNovoItem)
            {
                try
                {
                    PurchaseList compra = new PurchaseList();
                    bool isItemInexistente = true;                    
                    System.Console.WriteLine("Informe o item desejado e sua quantidade.");
                    System.Console.WriteLine("Usando: item \",\" quantidade. Ou: \"F\" para finalizar compra.");

                    string item = Console.ReadLine();

                    if (item.ToUpper().Equals("F"))
                        isNovoItem = false;
                    else
                    {
                        compra.item = item.Split(char.Parse(","))[0].ToUpper();
                        compra.qtdItem = item.Split(char.Parse(","))[1].ToUpper();

                        foreach (var d in _listaPrecos)
                        {
                            if (d.item.ToUpper().Equals(compra.item)){
                                isItemInexistente = false;
                                break;
                            }
                        }

                        if (!isItemInexistente)
                            listaCompras.Add(compra);
                        else
                            throw new Exception(string.Format("O item: {0} informado não existe na lista de compras.",compra.item));
                    }
                }
                catch(Exception erro)
                {
                    if (!erro.Message.Equals("O índice estava fora dos limites da matriz."))
                    {
                        System.Console.WriteLine(erro.Message);
                    }                    
                    System.Console.WriteLine("Valor informado inválido.");
                    continue;
                }
            }
                        
            return listaCompras;            
        }

        /// <summary>
        /// Ajusta a lista de preços de acordo com os novos valores promocionais
        /// </summary>
        private static void AjustaPrecos()
        {
            System.Console.WriteLine("Informe qual item sofrerá ajuste para Special Price: ");
            string item = Console.ReadLine();

            foreach (var i in _listaPrecos)
            {
                if (i.item.ToUpper().Equals(item.ToUpper()))
                {
                    System.Console.WriteLine("Informe a quantidade: ");
                    string qtd = Console.ReadLine();
                    i.tantosPorQtd = qtd;

                    System.Console.WriteLine("Informe o valor pela quantidade: ");
                    string val = Console.ReadLine();
                    i.tantosPorValor = val;

                    System.Console.WriteLine(string.Format("Special Price ajustado para :{0} ",item.ToUpper()));

                    break;
                }
            }

            Console.WriteLine(Environment.NewLine);            
        }

        /// <summary>
        /// Exibe opção de administrativo
        /// </summary>
        private static string ExibeMenu()
        {
            string opcao = null;
            System.Console.WriteLine(Environment.NewLine);
            System.Console.WriteLine("Pressione:");            
            System.Console.WriteLine("1 - Para alteração de valores de Special Price.");
            System.Console.WriteLine("2 - Para exibir lista de itens.");
            System.Console.WriteLine("3 - Para realizar uma compra.");
            System.Console.WriteLine("4 - Para finalizar o sistema.");
            System.Console.WriteLine(Environment.NewLine);
            System.Console.WriteLine("Informe a opção desejada: ");
            opcao = Console.ReadLine();

            return opcao;            
        }

        /// <summary>
        /// Monta Lista com valores Default
        /// </summary>
        private static void MontaLista()
        {
            PriceList itemA = new PriceList();
            itemA.item = "A";
            itemA.unitPrice = "50";            
            itemA.tantosPorQtd = "3";
            itemA.tantosPorValor = "130";
           
            _listaPrecos.Add(itemA);


            PriceList itemB = new PriceList();
            itemB.item = "B";
            itemB.unitPrice = "30";            
            itemB.tantosPorQtd = "2";
            itemB.tantosPorValor = "45";

            _listaPrecos.Add(itemB);


            PriceList itemC = new PriceList();
            itemC.item = "C";
            itemC.unitPrice = "20";
            itemC.specialPrice = null;

            _listaPrecos.Add(itemC);


            PriceList itemD = new PriceList();
            itemD.item = "D";
            itemD.unitPrice = "15";
            itemD.specialPrice = null;

            _listaPrecos.Add(itemD);


            PriceList itemO = new PriceList();
            itemO.item = "O";
            itemO.unitPrice = "  ";
            itemO.specialPrice = null;

            _listaPrecos.Add(itemO);
        }

        /// <summary>
        /// Lista os Preços Default
        /// </summary>
        private static void ListaPrecoAtual()
        {

            System.Console.WriteLine("############### Itens Disponíveis ####################");
            System.Console.WriteLine("Item | Unit Price | Special Price|");

            foreach (var d in _listaPrecos)
            {
                if (string.IsNullOrWhiteSpace(d.tantosPorQtd))
                    System.Console.WriteLine(string.Format("{0}    | {1}         |                ", d.item, d.unitPrice));
                else                
                    System.Console.WriteLine(string.Format("{0}    | {1}         | {2} for {3}    ", d.item, d.unitPrice, d.tantosPorQtd, d.tantosPorValor));
                
            }

            System.Console.WriteLine("######################################################");
        }
       
    }

    public class PriceList
    {
        public string item { get; set; }
        public string unitPrice { get; set; }
        public string specialPrice { get; set; }
        public string tantosPorQtd { get; set; }
        public string tantosPorValor { get; set; }
    }

    
    public class PurchaseList
    {
        public string item { get; set; }
        public string qtdItem { get; set; }
    }
}


Funcionalidade: Enviar localização ao grupo via WhatsApp
    COMO Candidato a vaga de QA
	QUERO Criar um BDD que envie minha localização atual via whatsapp
	PARA Demonstrar meu conhecimento em elicitar uma especificação viva
		
Esquema do Cenário: Enviar localização
	    DADO    Acesso ao "whatsapp"		
		QUANDO  Clico em <Grupos QA>		
		E       Clico em "Localização"
		E       Clico em "Enviar sua localização atual"
		ENTÃO   A localização atual deve se exibida ao grupo
	Exemplos: 
		|Grupos QA      |
		|Analistas      |
		|Automatizadores|

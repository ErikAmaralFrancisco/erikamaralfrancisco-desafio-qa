
Funcionalidade: Enviar mensagem de bom dia via WhatsApp
    COMO Candidato a vaga de QA
	QUERO Criar um BDD que envie uma mensagem de bom dia via whatsapp
	PARA Demonstrar meu conhecimento em elicitar uma especificação viva e executa-la com CUCUMBER
		
Esquema do Cenário: Enviar bom dia
	    DADO    Acesso ao "whatsapp"		
		QUANDO  Clico em <Grupos QA>
		E       Preencho "Mensagem" com "Bom dia"
		E       Clico em "Enviar"
		ENTÃO   A mensagem deve ser exbida ao grupo
	Exemplos: 
		|Grupos QA      |
		|Analistas      |
		|Automatizadores|